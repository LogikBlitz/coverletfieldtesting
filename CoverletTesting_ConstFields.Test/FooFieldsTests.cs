using System;
using FluentAssertions;
using Xunit;

namespace CoverletTesting_ConstFields.Test
{
    public class FooFieldsTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void FooField_Page_ShouldBeSpelled_As()
        {
            //ASSERT
            FooFields.Page.Should().Be("foo_page");
        }
        
        [Fact]
        [Trait("Category", "Unit")]
        public void FooField_Name_ShouldBeSpelled_As()
        {
            //ASSERT
            FooFields.Name.Should().Be("foo_name");
        }
        
        [Fact]
        [Trait("Category", "Unit")]
        public void FooField_Harry_ShouldBeSpelled_As()
        {
            //ASSERT
            FooFields.Harry.Should().Be("foo_harry");
        }
    }
}