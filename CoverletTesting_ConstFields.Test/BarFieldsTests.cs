using FluentAssertions;
using Xunit;

namespace CoverletTesting_ConstFields.Test
{
    public class BarFieldsTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void BarField_Page_ShouldBeSpelled_As()
        {
            //ASSERT
            BarFields.Page.Should().Be("bar_page");
        }
        
        [Fact]
        [Trait("Category", "Unit")]
        public void BarField_Name_ShouldBeSpelled_As()
        {
            //ASSERT
            BarFields.Name.Should().Be("bar_name");
        }
        
        [Fact]
        [Trait("Category", "Unit")]
        public void BarField_Harry_ShouldBeSpelled_As()
        {
            //ASSERT
            BarFields.Harry.Should().Be("bar_harry");
        }
    }
}